/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ex6g2;

import static java.lang.Math.abs;

/**
 *
 * @author user
 */
import java.util.Random;
import java.util.Scanner;

public class ex6G2 {

 public static void main(String[] argv){

     int x, num, diff,jog, ntentativas;
   
     Scanner ler = new Scanner(System.in);
     Random  rand= new Random();
     
     x = rand.nextInt(100);
     System.out.println("Quanto jogadores?");
     jog = ler.nextInt();
     
     do {
         
         System.out.println("Quantas tentativas para adevinhar entre 0 e 10 ?");
         ntentativas = ler.nextInt();
         
     } while (ntentativas < 0 || ntentativas > 10 );
     
     for (int i = 0; i < jog; i++) {
                 
        for (int j = 0; j < ntentativas; j++) {              
            do {
         
                System.out.println("Jogador "+(i+1)+" adivinhe um número inteiro entre 0 e 100?");
                num = ler.nextInt();
         
            } while (num < 0 || num > 100 );
         
            diff = num - x;
     
            if (abs(diff)>25){
            System.out.println("Frio");}
     
            else if(abs(diff)< 5){
            System.out.println("Quente");
                }
            else{
            System.out.println("Morno");
        }
        }
   }
 }
    
}
